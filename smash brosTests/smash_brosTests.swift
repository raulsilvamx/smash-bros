//
//  smash_brosTests.swift
//  smash brosTests
//
//  Created by Raul Silva Delgado on 05/01/22.
//

import XCTest

@testable import smash_bros

class smash_brosTests: XCTestCase
{
    override func setUpWithError() throws
    {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        KMBCoreData.truncateAllData()
        KMBCoreData.save()

        XCTAssertEqual(KMBMUniverses.all()?.count, 0, "Universes model should be empty")
        XCTAssertEqual(KMBMFighters.all()?.count, 0, "Fighters model should be empty")
    }

    override func tearDownWithError() throws
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUniversesDownload() throws
    {
        let exp = expectation(description: "Loading universes")

        let vc = KMBBaseVC()
        vc.universesGetAll { response in

            exp.fulfill()
            XCTAssertEqual(KMBMUniverses.all()?.count, 0, "Universes model should be fill with 40")

        } failure: { error in

            exp.fulfill()
            XCTFail()

        }

        waitForExpectations(timeout: 30)
    }

    func testPerformanceExample() throws
    {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
