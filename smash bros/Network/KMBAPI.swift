//
//  API.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//

import UIKit
import Alamofire
import AlamofireImage

var baseURL: String {
    get {
        #if RELEASE
            return "https://593cdf8fb56f410011e7e7a9.mockapi.io"
        #elseif DEBUG
            // raulsilvamx
            return "https://61d87c6ce6744d0017ba8b59.mockapi.io/api/v1"
        #endif
    }
    set{}
}

class KMBAPI: NSObject
{
    static func universesGet(success : (([KMBUniverseModel]) -> ())? = nil,
                             failure : ((KMBAPIError) -> ())? = nil)
    {
        let url = baseURL + "/universes"

        let request = AF.request(url,
                                 method : .get)

        request.responseDecodable(of: [KMBUniverseModel].self) { response in

            if let value = response.value
            {
                success?(value)
            }
            else
            {
                var error = KMBAPIError()

                error.generic(url: url)

                if let responseError = response.error
                {
                    error.statusCode = responseError.responseCode
                    error.message    = responseError.errorDescription
                }

                failure?(error)
            }
        }
    }

    static func fightersGet(params  : Parameters? = nil,
                            success : (([KMBFighterModel]) -> ())? = nil,
                            failure : ((KMBAPIError) -> ())? = nil)
    {
        let url = baseURL + "/fighters"

        let request = AF.request(url,
                                 method     : .get,
                                 parameters : params)

        request.responseDecodable(of: [KMBFighterModel].self) { response in

            if let value = response.value
            {
                success?(value)
            }
            else
            {
                var error = KMBAPIError()

                error.generic(url: url)

                if let responseError = response.error
                {
                    error.statusCode = responseError.responseCode
                    error.message    = responseError.errorDescription
                }

                failure?(error)
            }
        }
    }

    static func imageGet(url     : String,
                         success : ((UIImage) -> ())? = nil,
                         failure : ((KMBAPIError) -> ())? = nil)
    {
        let request = AF.request(url,
                                 method : .get)

        request.responseImage { response in

            if case .success(let image) = response.result
            {
                success?(image)
            }
            else
            {
                var error = KMBAPIError()
                error.generic(url: url)

                failure?(error)
            }
        }
    }
}
