//
//  KMBAPIError.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

struct KMBAPIError: Decodable
{
    var status     : String?
    var statusCode : Int?
    var message    : String?
    var exception  : String?
    var url        : String?

    enum CodingKeys: String, CodingKey
    {
        case status     = "status"
        case statusCode = "statusCode"
        case message    = "message"
        case exception  = "exception"
        case url        = "URL"
    }

    mutating func generic(url: String)
    {
        status     = "ERROR"
        statusCode = 401
        message    = "Default Error"
        exception  = "DEFAULT"
        self.url   = url
    }
}
