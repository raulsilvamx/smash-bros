//
//  KMBUniverseModel.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

struct KMBUniverseModel: Decodable
{
    var objectId          : String?
    var name              : String?
    var descriptionString : String?

    enum CodingKeys: String, CodingKey
    {
        case objectId          = "objectID"
        case name              = "name"
        case descriptionString = "description"
    }
}
