//
//  KMBFighterModel.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

struct KMBFighterModel: Decodable
{
    var objectId          : String?
    var name              : String?
    var universeName      : String?
    var price             : String?
    var popular           : Bool?
    var rate              : Int?
    var downloads         : String?
    var descriptionString : String?
    var createdAt         : String?
    var imageUrl          : String?

    enum CodingKeys: String, CodingKey
    {
        case objectId          = "objectID"
        case name              = "name"
        case universeName      = "universe"
        case price             = "price"
        case popular           = "popular"
        case rate              = "rate"
        case downloads         = "downloads"
        case descriptionString = "description"
        case createdAt         = "created_at"
        case imageUrl          = "imageURL"
    }
}
