//
//  AppDelegate.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//

import UIKit
import CoreData
import AERecord

@main
class AppDelegate: UIResponder, UIApplicationDelegate
{
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        KMBLog.separador(force: true)
        KMBLog.p(message: "App         : Smash Bros", force: true)
        KMBLog.p(message: "Target      : \(String.appTargetName())", force: true)
        KMBLog.p(message: "Version     : \(String.appVersionBuild())", force: true)
        KMBLog.p(message: "Device Size : \(UIDevice.current.screenType.rawValue)", force: true)
        KMBLog.separador(force: true)

        #if RELEASE
            KMBLog.p(message: "Release Environment", force: true)
            KMBLog.separador(force: true)

        #elseif DEBUG
            KMBLog.p(message: "Debug Environment", force: true)
            KMBLog.separador(force: true)

        #endif

        KMBLog.p(message: "BaseURL: \(baseURL)", force: true)
        KMBLog.separador(force: true)


        // AERecord

        KMBCoreData.setup()

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

