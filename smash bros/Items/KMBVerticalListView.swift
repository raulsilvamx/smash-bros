//
//  KMBHorizontalSelectorView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import SnapKit

class KMBVerticalListView: UIView
{
    var model: [KMBMFighters]? {
        didSet {
            //KMBLog.p(message: "KMBHorizontalSelectorView model count: \(model?.count ?? 0)")
            collectionView.reloadDataMainThread()
        }
    }

    var modelSelectedIndexPath : IndexPath = IndexPath(row: 0, section: 0) {
        didSet {
            //KMBLog.p(message: "KMBHorizontalSelectorView modelSelectedIndexPath: \(modelSelectedIndexPath)")
            collectionView.reloadDataMainThread()
        }
    }

    var modelSelectedClosure : ((KMBMFighters?) -> ())?

    lazy var collectionView : UICollectionView = { [unowned self] in

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()

        let size = (mainBounds.width / 2) - 20

        layout.scrollDirection = .vertical
        layout.sectionInset    = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize        = CGSize(width: size, height: size * 1.1)


        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)

        collectionView.showsVerticalScrollIndicator   = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = KMBColor.white.asColor()
        collectionView.register(KMBFighterCell.self, forCellWithReuseIdentifier: "KMBFighterCell")
        collectionView.delegate   = self
        collectionView.dataSource = self

        return collectionView
    }()

    // MARK: - Inits

    init()
    {
        super.init(frame: CGRect(x: 0,
                                 y: 0,
                                 width : mainBounds.width,
                                 height: 50))
        setup()
    }

    /// Init for using in code
    /// - Parameter frame: CGRect
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    /// Init for using in IB
    /// - Parameter coder: NSCoder
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func setup()
    {
        addSubview(collectionView)
        collectionView.snp.remakeConstraints { make in
            make.left.top.right.bottom.equalToSuperview()
        }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate

extension KMBVerticalListView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return model?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KMBFighterCell", for: indexPath) as! KMBFighterCell

        if let model = model?[indexPath.row]
        {
            cell.model = model

            cell.buttonClosure = { [unowned self] model in

                modelSelectedIndexPath = indexPath
                modelSelectedClosure?(model)
            }
        }

        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        scrollView.contentOffset.x = 0.0
    }
}
