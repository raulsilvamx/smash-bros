//
//  KMBTitleSeparatorView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBTitleSeparatorView: UIView
{
    lazy var titleLabel: UILabel = {

        let label = UILabel()

        label.font      = KMBFont.helveticaNeueBold.asFont()
        label.textColor = KMBColor.black.asColor()

        return label
    }()

    lazy var separator: UIView = {

        let view = UIView()

        view.backgroundColor = KMBColor.separatorGray.asColor()

        return view
    }()


    // MARK: - Inits

    init()
    {
        super.init(frame: CGRect(x: 0,
                                 y: 0,
                                 width : mainBounds.width,
                                 height: 50))
        setup()
    }

    /// Init for using in code
    /// - Parameter frame: CGRect
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    /// Init for using in IB
    /// - Parameter coder: NSCoder
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func setup()
    {
        addSubview(titleLabel)
        titleLabel.snp.remakeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.top.bottom.equalToSuperview()
            make.width.greaterThanOrEqualTo(0)
        }

        addSubview(separator)
        separator.snp.remakeConstraints { [unowned self] make in
            make.centerY.equalToSuperview()
            make.left.equalTo(self.titleLabel.snp.right).offset(5)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(1)
        }
    }

    // MARK: - Update

    func update(title: String,
                count: Int)
    {
        titleLabel.text = "\(title) (\(count))"
    }
}
