//
//  KMBRateView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

enum KMBRateViewColor
{
    case orange
    case white
}

class KMBRateView: UIView
{
    let STAR_NUMBER = 5

    var mode: KMBRateViewColor = .orange

    var buttons: [UIButton] = []

    var buttonClosure : ((Int) -> ())?

    // MARK: - Inits

    init(width : CGFloat,
         mode  : KMBRateViewColor = .orange)
    {
        super.init(frame: CGRect(x: 0,
                                 y: 0,
                                 width : width,
                                 height: 50))

        self.mode = mode

        setup()
    }

    /// Init for using in code
    /// - Parameter frame: CGRect
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    /// Init for using in IB
    /// - Parameter coder: NSCoder
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func setup()
    {
        buttonsInit()

        for index in Array(0..<STAR_NUMBER)
        {
            let widthButton = frame.width / CGFloat(STAR_NUMBER)

            addSubview(buttons[index])
            buttons[index].snp.remakeConstraints { make in
                make.left.equalToSuperview().offset(widthButton * CGFloat(index))
                make.width.equalTo(widthButton)
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
            }
        }

        buttonsAllToNormal()
    }

    // MARK: - Buttons

    func buttonsInit()
    {
        buttons.removeAll()

        for index in Array(0..<STAR_NUMBER)
        {
            let button = UIButton()

            button.startStyle()
            button.tag = index

            button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

            buttons.append(button)
        }
    }

    func buttonsDisable()
    {
        for button in buttons
        {
            button.isUserInteractionEnabled = false
        }
    }

    func buttonsEnable()
    {
        for button in buttons
        {
            button.isUserInteractionEnabled = true
        }
    }

    func buttonSelected(index: Int)
    {
        let button = buttons[index]
        button.isSelected = true

        switch mode
        {
        case .orange:
            button.tintColor = KMBColor.starOrange.asColor()

        case .white:
            button.tintColor = KMBColor.white.asColor()
        }
    }

    func buttonsSelectedTo(index: Int)
    {
        for i in Array(0..<STAR_NUMBER)
        {
            if i <= index
            {
                buttonSelected(index: i)
            }
            else
            {
                buttonNormal(index: i)
            }
        }
    }

    func buttonsAllToNormal()
    {
        for i in Array(0..<STAR_NUMBER)
        {
            buttonNormal(index: i)
        }
    }

    func buttonNormal(index: Int)
    {
        let button = buttons[index]
        button.isSelected = false

        switch mode
        {
        case .orange:
            button.tintColor = KMBColor.black.asColor().withAlphaComponent(0.3)

        case .white:
            button.tintColor = KMBColor.black.asColor().withAlphaComponent(0.3)
        }
    }

    @objc func buttonAction(_ sender: Any)
    {
        let button = sender as! UIButton

        buttonsSelectedTo(index: button.tag)

        buttonClosure?(button.tag)
    }

    // MARK: - Getters

    func getValue() -> Int
    {
        var value = 0

        for i in Array(0..<STAR_NUMBER)
        {
            let button = buttons[i]

            if button.isSelected
            {
                value += 1
            }
            else
            {
                break
            }
        }

        return value
    }
}
