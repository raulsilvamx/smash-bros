//
//  KMBHorizontalButton.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBHorizontalButton: UIView
{
    var isSelected = false {
        didSet {
            updateStyle()
        }
    }

    var buttonClosure: ((Int) -> ())?

    lazy var titleLabel: UILabel = {

        let label = UILabel()

        label.font          = KMBFont.helveticaNeueNormal.asFont(size: 14)
        label.textColor     = KMBColor.black.asColor().withAlphaComponent(0.9)
        label.textAlignment = .left

        return label
    }()

    lazy var iconBullet: UIImageView = {

        let imageView = UIImageView()

        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    lazy var button: UIButton = {

        let button = UIButton()

        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

        return button
    }()

    // MARK: - Inits

    init()
    {
        super.init(frame: CGRect(x: 0,
                                 y: 0,
                                 width : mainBounds.width,
                                 height: 50))

        setup()
    }

    /// Init for using in code
    /// - Parameter frame: CGRect
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    /// Init for using in IB
    /// - Parameter coder: NSCoder
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func setup()
    {
        addSubview(iconBullet)
        iconBullet.snp.remakeConstraints { make in
            make.centerY.equalToSuperview()
            make.width.height.equalTo(35)
            make.right.equalToSuperview()
        }

        addSubview(titleLabel)
        titleLabel.snp.remakeConstraints { [unowned self] make in
            make.left.top.bottom.equalToSuperview()
            make.right.equalTo(self.iconBullet.snp.left).offset(-5)
        }

        addSubview(button)
        button.snp.remakeConstraints { make in
            make.left.top.right.bottom.equalToSuperview()
        }

        updateStyle()
    }

    // MARK: - Actions

    @objc func buttonAction(_ sender: Any)
    {
        let button = sender as! UIButton
        buttonClosure?(button.tag)
    }

    // MARK: - Update

    func updateStyle()
    {
        if isSelected
        {
            iconBullet.image = UIImage(systemName: "record.circle")?.withRenderingMode(.alwaysTemplate)
            iconBullet.tintColor = KMBColor.filterSelected.asColor()
        }
        else
        {
            iconBullet.image = UIImage(systemName: "circle")?.withRenderingMode(.alwaysTemplate)
            iconBullet.tintColor = KMBColor.filterUnselected.asColor()
        }
    }
}
