//
//  KMBHorizontalSelectorView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import SnapKit

class KMBHorizontalSelectorView: UIView
{
    var model: [KMBMUniverses]? {
        didSet {
            //KMBLog.p(message: "KMBHorizontalSelectorView model count: \(model?.count ?? 0)")
            collectionView.reloadDataMainThread()
        }
    }

    var modelSelectedIndexPath : IndexPath = IndexPath(row: 0, section: 0) {
        didSet {
            //KMBLog.p(message: "KMBHorizontalSelectorView modelSelectedIndexPath: \(modelSelectedIndexPath)")
            collectionView.reloadDataMainThread()
        }
    }

    var modelSelectedClosure : ((KMBMUniverses?) -> ())?

    lazy var collectionView : UICollectionView = { [unowned self] in

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()

        layout.scrollDirection = .horizontal
        layout.sectionInset    = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize        = CGSize(width: mainBounds.width / 2.5, height: 50)


        var collectionViewFrame    = mainBounds

        collectionViewFrame.size.height = 50


        let collectionView = UICollectionView(frame: collectionViewFrame, collectionViewLayout: layout)

        collectionView.showsVerticalScrollIndicator   = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = KMBColor.white.asColor()
        collectionView.register(KMBUniverseSelectorCell.self, forCellWithReuseIdentifier: "KMBUniverseSelectorCell")
        collectionView.delegate   = self
        collectionView.dataSource = self

        return collectionView
    }()

    // MARK: - Inits

    init()
    {
        super.init(frame: CGRect(x: 0,
                                 y: 0,
                                 width : mainBounds.width,
                                 height: 50))
        setup()
    }

    /// Init for using in code
    /// - Parameter frame: CGRect
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    /// Init for using in IB
    /// - Parameter coder: NSCoder
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func setup()
    {
        addSubview(collectionView)
        collectionView.snp.remakeConstraints { make in
            make.left.top.right.bottom.equalToSuperview()
        }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate

extension KMBHorizontalSelectorView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (model?.count ?? 0 + 1)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KMBUniverseSelectorCell", for: indexPath) as! KMBUniverseSelectorCell

        if indexPath.row == 0
        {
            cell.updateWith(string: "All")

            cell.buttonClosure = { [unowned self] model in

                modelSelectedIndexPath = indexPath
                modelSelectedClosure?(nil)
            }
        }
        else if let model = model?[indexPath.row - 1]
        {
            cell.model = model

            cell.buttonClosure = { [unowned self] model in

                modelSelectedIndexPath = indexPath
                modelSelectedClosure?(model)
            }
        }

        if modelSelectedIndexPath == indexPath
        {
            cell.mode = .selected
        }
        else
        {
            cell.mode = .normal
        }


        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        scrollView.contentOffset.y = 0.0
    }
}
