// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to KMBMFighters.swift instead.

import Foundation
import CoreData

public enum KMBMFightersAttributes: String {
    case createdAt = "createdAt"
    case descriptionString = "descriptionString"
    case downloads = "downloads"
    case imageUrl = "imageUrl"
    case name = "name"
    case objectId = "objectId"
    case popular = "popular"
    case price = "price"
    case rate = "rate"
    case universeName = "universeName"
}

public enum KMBMFightersRelationships: String {
    case universe = "universe"
}

open class _KMBMFighters: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "KMBMFighters"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<KMBMFighters> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _KMBMFighters.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var createdAt: String?

    @NSManaged open
    var descriptionString: String?

    @NSManaged open
    var downloads: Int64 // Optional scalars not supported

    @NSManaged open
    var imageUrl: String?

    @NSManaged open
    var name: String?

    @NSManaged open
    var objectId: String?

    @NSManaged open
    var popular: Bool // Optional scalars not supported

    @NSManaged open
    var price: Double // Optional scalars not supported

    @NSManaged open
    var rate: Int16 // Optional scalars not supported

    @NSManaged open
    var universeName: String?

    // MARK: - Relationships

    @NSManaged open
    var universe: KMBMUniverses?

}

