import Foundation
import UIKit

@objc(KMBMUniverses)
open class KMBMUniverses: _KMBMUniverses
{
    static func getAllSorted() -> [KMBMUniverses]?
    {
        let sortBy = NSSortDescriptor(key: "name", ascending: true)
        return KMBMUniverses.all(orderedBy: [sortBy]) as? [KMBMUniverses]
    }

    func fill(model: KMBUniverseModel)
    {
        objectId          = model.objectId
        name              = model.name
        descriptionString = model.descriptionString
        cached            = false
    }
}
