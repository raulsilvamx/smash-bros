//
//  KMBCoreData.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 07/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import AERecord

class KMBCoreData: NSObject
{
    static func setup()
    {
        do {
            try AERecord.loadCoreDataStack()
        } catch {
            KMBLog.p(message: "AERecord error: \(error)", force: true)
        }
    }

    static func truncateAllData()
    {
        AERecord.truncateAllData()
    }

    static func save()
    {
        AERecord.save()
    }
}
