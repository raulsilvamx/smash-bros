//
//  KMBFightersSorterModel.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 06/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

enum KMBFightersSorterType : String
{
    case name
    case price
    case rate
    case downloads
}

struct KMBFightersSorterModel
{
    var sorters: [KMBFightersSorterType] = []

    func toSortDescriptor() -> [NSSortDescriptor]
    {
        var sortDesciptors : [NSSortDescriptor] = []

        for sort in sorters
        {
            sortDesciptors.append(NSSortDescriptor(key: sort.rawValue, ascending: true))
        }

        return sortDesciptors
    }
}
