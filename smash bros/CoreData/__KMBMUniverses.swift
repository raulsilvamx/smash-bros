// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to KMBMUniverses.swift instead.

import Foundation
import CoreData

public enum KMBMUniversesAttributes: String {
    case descriptionString = "descriptionString"
    case name = "name"
    case objectId = "objectId"
}

public enum KMBMUniversesRelationships: String {
    case fighters = "fighters"
}

open class _KMBMUniverses: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "KMBMUniverses"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<KMBMUniverses> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _KMBMUniverses.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var descriptionString: String?

    @NSManaged open
    var name: String?

    @NSManaged open
    var objectId: String?

    // MARK: - Relationships

    @NSManaged open
    var fighters: NSSet

    open func fightersSet() -> NSMutableSet {
        return self.fighters.mutableCopy() as! NSMutableSet
    }

}

extension _KMBMUniverses {

    open func addFighters(_ objects: NSSet) {
        let mutable = self.fighters.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.fighters = mutable.copy() as! NSSet
    }

    open func removeFighters(_ objects: NSSet) {
        let mutable = self.fighters.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.fighters = mutable.copy() as! NSSet
    }

    open func addFightersObject(_ value: KMBMFighters) {
        let mutable = self.fighters.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.fighters = mutable.copy() as! NSSet
    }

    open func removeFightersObject(_ value: KMBMFighters) {
        let mutable = self.fighters.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.fighters = mutable.copy() as! NSSet
    }

}

