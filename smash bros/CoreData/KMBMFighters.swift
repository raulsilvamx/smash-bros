import Foundation
import UIKit

@objc(KMBMFighters)
open class KMBMFighters: _KMBMFighters
{
    static func getAllSorted(filter: KMBFightersFilterModel) -> [KMBMFighters]?
    {
        if filter.toPredicate() != nil
        {
            return KMBMFighters.all(with: filter.toPredicate()!,
                                    orderedBy: filter.toSortBy()) as? [KMBMFighters]
        }
        else
        {
            return KMBMFighters.all(orderedBy: filter.toSortBy())
        }
    }

    func imageGet(success: ((UIImage?) -> ())? = nil,
                  failure: ((KMBAPIError) -> ())? = nil)
    {
        if let objectId = objectId
        {
            if let image = UIImage.getFromCache(id: objectId)
            {
                success?(image)
            }
            else
            {
                if let imageUrl = imageUrl
                {
                    KMBAPI.imageGet(url: imageUrl) { image in

                        image.saveToCache(id: objectId)
                        success?(image)

                    } failure: { error in

                        KMBLog.p(message: "\(error)")
                        failure?(error)
                    }
                }
                else
                {
                    let error = KMBAPIError()
                    failure?(error)
                }
            }
        }
    }

    func fill(model: KMBFighterModel)
    {
        objectId          = model.objectId
        name              = model.name
        universeName      = model.universeName
        price             = Double(model.price ?? "0.0")!
        popular           = model.popular ?? false
        rate              = Int16(model.rate ?? 0)
        downloads         = Int64(Int(model.downloads ?? "0") ?? 0)
        descriptionString = model.descriptionString
        createdAt         = model.createdAt
        imageUrl          = model.imageUrl
    }
}
