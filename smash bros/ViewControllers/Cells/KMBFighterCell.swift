//
//  KMBFighterCell.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBFighterCell: UICollectionViewCell
{
    var model: KMBMFighters? {
        didSet {
            updateWithModel()
        }
    }

    var buttonClosure : ((KMBMFighters?) -> ())?

    lazy var fighterImage: UIImageView = {

        let imageView = UIImageView()

        imageView.contentMode     = .scaleAspectFit
        imageView.backgroundColor = KMBColor.fighterImageBg.asColor()
        imageView.roundedStyle()
        imageView.borderGrayLight()

        return imageView
    }()

    lazy var button : UIButton = {

        let button = UIButton()

        button.setTitle(nil, for: .normal)
        button.titleLabel?.font = KMBFont.helveticaNeueNormal.asFont(size: 14)
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

        return button
    }()

    lazy var titleLabel: UILabel = {

        let label = UILabel()

        label.font          = KMBFont.helveticaNeueBold.asFont()
        label.textAlignment = .center
        label.numberOfLines = 0

        return label
    }()

    // MARK: - Inits

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func viewSetup()
    {
        backgroundColor = .clear
    }

    func setup()
    {
        addSubview(titleLabel)
        titleLabel.snp.remakeConstraints { make in
            make.left.bottom.right.equalToSuperview()
            make.height.greaterThanOrEqualTo(0)
        }

        addSubview(fighterImage)
        fighterImage.snp.remakeConstraints { [unowned self] make in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(self.titleLabel.snp.top).offset(-5)
        }

        addSubview(button)
        button.snp.remakeConstraints { make in
            make.left.top.right.bottom.equalToSuperview()
        }
    }

    // MARK: - Button

    @objc func buttonAction(_ sender: Any)
    {
        buttonClosure?(model)
    }

    // MARK: - Update

    func updateWith(string: String)
    {
        button.setTitle(string, for: .normal)
    }

    func updateWithModel()
    {
        if let model = model
        {
            fighterImage.image = nil

            model.imageGet { [unowned self] image in
                fighterImage.image = image
            } failure: { error in
                KMBLog.p(message: "\(error)")
            }

            titleLabel.fighter(name     : model.name ?? "",
                               universe : model.universeName ?? "")
        }
    }
}
