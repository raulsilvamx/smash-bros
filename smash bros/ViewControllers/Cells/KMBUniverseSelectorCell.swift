//
//  KMBUniverseSelectorCell.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import SnapKit

enum KMBHorizontalSelectorMode : Int
{
    case selected
    case normal
}

class KMBUniverseSelectorCell: UICollectionViewCell
{
    var model: KMBMUniverses? {
        didSet {
            updateWithModel()
        }
    }

    var mode: KMBHorizontalSelectorMode = .normal {
        didSet {
            updateMode()
        }
    }

    var buttonClosure : ((KMBMUniverses?) -> ())?

    lazy var button : UIButton = {

        let button = UIButton()

        button.titleLabel?.font = KMBFont.helveticaNeueNormal.asFont(size: 14)
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)

        return button
    }()

    // MARK: - Inits

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func viewSetup()
    {
        backgroundColor = .clear
    }

    func setup()
    {
        addSubview(button)
        button.snp.remakeConstraints { make in
            make.left.top.right.bottom.equalToSuperview()
        }

        updateMode()
    }

    // MARK: - Button

    @objc func buttonAction(_ sender: Any)
    {
        buttonClosure?(model)
    }

    // MARK: - Update

    func updateWith(string: String)
    {
        button.setTitle(string, for: .normal)
    }

    func updateWithModel()
    {
        if let model = model
        {
            button.setTitle(model.name, for: .normal)
        }
    }

    func updateMode()
    {
        mainThread { [unowned self] in

            switch mode
            {
            case .selected:
                button.redSelectedStyle()
                break

            case .normal:
                button.redNormalStyle()
                break
            }
        }
    }
}
