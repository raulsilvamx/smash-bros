//
//  KMBFightersListVC.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import SnapKit

class KMBFightersListVC: KMBBaseVC
{
    var filter = KMBFightersFilterModel()

    var universeModel : KMBMUniverses?

    lazy var refreshControl : UIRefreshControl = {

        let refresh = UIRefreshControl()

        refresh.addTarget(self, action: #selector(refreshAllData(_:)), for: .valueChanged)

        return refresh
    }()

    lazy var universesSelector : KMBHorizontalSelectorView = {

        let selector = KMBHorizontalSelectorView()

        selector.model = KMBMUniverses.getAllSorted()
        selector.modelSelectedClosure = { [unowned self] model in

            universeModel = model
            update(model: universeModel)
        }

        return selector
    }()

    lazy var titleSeparator: KMBTitleSeparatorView = {

        let separatorView = KMBTitleSeparatorView()

        separatorView.backgroundColor = KMBColor.white.asColor()

        return separatorView
    }()

    lazy var fightersList: KMBVerticalListView = {

        let selector = KMBVerticalListView()

        selector.collectionView.refreshControl = refreshControl

        selector.model = KMBMFighters.getAllSorted(filter: filter)
        selector.modelSelectedClosure = { [unowned self] model in

            if let model = model
            {
                showKMBFighterDetailVC(model: model)
            }
        }

        return selector
    }()

    // MARK: - View Cycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        setup()

        universeModel = nil
        update(model: universeModel)
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        navigationSetup()

        navigationLeftButtonHide()
        navigationRightButton(image: UIImage(systemName: "line.3.horizontal.decrease.circle"))

        navigationItem.setHidesBackButton(true, animated: true)

        title = "Fighters"
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }


    // MARK: - Setup

    func viewSetup()
    {
        view.backgroundColor = KMBColor.white.asColor()
    }

    func setup()
    {
        viewSetup()

        view.addSubview(universesSelector)
        universesSelector.snp.remakeConstraints { make in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(60)
        }

        view.addSubview(titleSeparator)
        titleSeparator.snp.remakeConstraints { make in
            make.top.equalTo(self.universesSelector.snp.bottom).offset(5)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(50)
        }

        view.addSubview(fightersList)
        fightersList.snp.remakeConstraints { make in
            make.top.equalTo(self.titleSeparator.snp.bottom).offset(5)
            make.left.bottom.right.equalToSuperview()
        }
    }

    // MARK: - Navigator

    override func navigationRightAction(_ sender: Any)
    {
        showKMBFighterFilterVC(model: filter) { [unowned self] model in

            if let model = model
            {
                filter = model
            }

            update(model: universeModel)
        }
    }

    // MARK: - Update

    func update(model: KMBMUniverses?)
    {
        if let model = model
        {
            filter.universe = model.name
        }
        else
        {
            filter.universe = nil
        }

        fightersList.model = KMBMFighters.getAllSorted(filter: filter)
        titleSeparator.update(title: "Fighters", count: fightersList.model?.count ?? 0)
    }

    // MARK: - Refresh

    @objc func refreshAllData(_ sender: Any)
    {
        universesSelector.model = nil
        universesSelector.modelSelectedIndexPath = IndexPath(row: 0, section: 0)

        titleSeparator.update(title: "Fighters", count: 0)

        fightersList.model = nil

        universesAndFightersToCacheFreshData { [unowned self] in

            filter = KMBFightersFilterModel()

            universesSelector.model = KMBMUniverses.getAllSorted()
            universesSelector.modelSelectedIndexPath = IndexPath(row: 0, section: 0)

            fightersList.model = KMBMFighters.getAllSorted(filter: filter)
            fightersList.collectionView.refreshControl?.endRefreshing()

            titleSeparator.update(title: "Fighters", count: fightersList.model?.count ?? 0)

        } failure: { [unowned self] error in
            KMBLog.p(message: "\(error)")
            fightersList.collectionView.refreshControl?.endRefreshing()
        }
    }
}

extension UIViewController
{
    func showKMBFightersListVC()
    {
        let vc   = KMBFightersListVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}
