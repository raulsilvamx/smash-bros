//
//  KMBRootVC.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBRootVC: KMBBaseVC
{
    var page = 0 {
        didSet {
            updatePage()
        }
    }

    lazy var bgFullImageView: UIImageView = {

        let imageView = UIImageView()

        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "bg-full")

        return imageView
    }()

    lazy var characterImageView: UIImageView = {

        let imageView = UIImageView()

        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    lazy var titleLabel: UILabel = {

        let label = UILabel()

        label.font          = KMBFont.helveticaNeueNormal.asFont(size: 28)
        label.textColor     = KMBColor.white.asColor()
        label.textAlignment = .center
        label.numberOfLines = 0

        return label
    }()

    lazy var continueButton: UIButton = {

        let button = UIButton()

        button.redSelectedStyle()
        button.roundedStyle(round: 20)
        button.addTarget(self, action: #selector(continueButtonAction(_:)), for: .touchUpInside)

        return button
    }()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup

    func setup()
    {
        view.addSubview(bgFullImageView)
        bgFullImageView.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(-10)
            make.left.right.bottom.equalToSuperview()
        }

        view.addSubview(characterImageView)
        characterImageView.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(150)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(characterImageView.snp.width)
        }

        view.addSubview(titleLabel)
        titleLabel.snp.remakeConstraints { make in
            make.top.equalTo(self.characterImageView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.greaterThanOrEqualTo(0)
        }

        view.addSubview(continueButton)
        continueButton.snp.remakeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(mainBounds.width * 0.6)
            make.height.equalTo(50)
            make.bottom.equalToSuperview().offset(-60)
        }

        updatePage()
    }

    // MARK: - Actions

    @objc func continueButtonAction(_ sender: Any)
    {
        if page < 2
        {
            page += 1
        }
        else
        {
            activityIndicatorShow()
            universesAndFightersToCache { [unowned self] in

                activityIndicatorHide()
                showKMBFightersListVC()

            } failure: { [unowned self] error in

                activityIndicatorHide()
                KMBLog.p(message: "error")
            }
        }
    }

    // MARK: - Update

    func updatePage()
    {
        if (KMBMFighters.getAllSorted(filter: KMBFightersFilterModel())?.count ?? 0) > 0
        {
            showKMBFightersListVC()
        }
        else if page == 0
        {
            characterImageView.image = UIImage(named: "nino")
            titleLabel.text = "Access our\nExtended Catalog"
            continueButton.setTitle("Next", for: .normal)
        }
        else if page == 1
        {
            characterImageView.image = UIImage(named: "sonic")
            titleLabel.text = "Filter Universes"
            continueButton.setTitle("Next", for: .normal)
        }
        else if page == 2
        {
            characterImageView.image = UIImage(named: "terry-bogard")
            titleLabel.text = "And More..."
            continueButton.setTitle("Get Stared", for: .normal)
        }
    }
}
