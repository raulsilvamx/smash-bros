//
//  KMBFighterDetailVC.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBFighterDetailVC: KMBBaseVC
{
    var model: KMBMFighters? {
        didSet {
            updateWithModel()
        }
    }

    lazy var fighterBackground: UIImageView = {

        let imageView = UIImageView()

        imageView.image       = UIImage(named: "bg-detail")
        imageView.contentMode = .scaleAspectFill

        return imageView
    }()

    lazy var fighterName: UILabel = {

        let label = UILabel()

        label.font          = KMBFont.helveticaNeueBold.asFont(size: 24)
        label.textColor     = KMBColor.white.asColor()
        label.textAlignment = .left

        return label
    }()

    lazy var fighterUniverse: UILabel = {

        let label = UILabel()

        label.font          = KMBFont.helveticaNeueMedium.asFont(size: 19)
        label.textColor     = KMBColor.white.asColor().withAlphaComponent(0.5)
        label.textAlignment = .left

        return label
    }()

    lazy var fighterDownloads: UILabel = {

        let label = UILabel()

        label.font          = KMBFont.helveticaNeueBold.asFont(size: 14)
        label.textColor     = KMBColor.white.asColor()
        label.textAlignment = .left

        return label
    }()

    lazy var fighterRate: KMBRateView = {

        let rateView = KMBRateView(width: mainBounds.width / 3, mode: .white)

        rateView.buttonsDisable()

        return rateView
    }()

    lazy var fighterPrice: UILabel = {

        let label = UILabel()

        label.backgroundColor = KMBColor.white.asColor()
        label.font            = KMBFont.helveticaNeueBold.asFont(size: 22)
        label.textColor       = KMBColor.main.asColor()
        label.textAlignment  = .center
        label.roundedStyle()

        return label
    }()

    lazy var fighterImage: UIImageView = {

        let imageView = UIImageView()

        imageView.contentMode     = .scaleAspectFit
        imageView.backgroundColor = .clear

        return imageView
    }()

    lazy var fighterDescription: UITextView = {

        let textView = UITextView()

        textView.font          = KMBFont.helveticaNeueNormal.asFont(size: 14)
        textView.textColor     = KMBColor.fighterText.asColor()
        textView.textAlignment = .justified
        textView.isEditable    = false
        textView.isSelectable  = false

        return textView
    }()


    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func viewSetup()
    {
        navigationTransparentSetup()
        navigationLeftButton(image: UIImage(systemName: "chevron.backward"))

        navigationItem.backButtonTitle = ""
        navigationItem.leftBarButtonItem?.tintColor = KMBColor.white.asColor()

        title = ""
    }

    func setup()
    {
        viewSetup()

        view.addSubview(fighterBackground)
        fighterBackground.snp.remakeConstraints { make in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(340)
        }

        view.addSubview(fighterName)
        fighterName.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(topBarHeight)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.greaterThanOrEqualTo(0)
        }

        view.addSubview(fighterUniverse)
        fighterUniverse.snp.remakeConstraints { make in
            make.top.equalTo(self.fighterName.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.greaterThanOrEqualTo(0)
        }

        view.addSubview(fighterDownloads)
        fighterDownloads.snp.remakeConstraints { make in
            make.top.equalTo(self.fighterUniverse.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.right.equalTo(mainBounds.width).multipliedBy(0.5)
            make.height.greaterThanOrEqualTo(0)
        }

        view.addSubview(fighterRate)
        fighterRate.snp.remakeConstraints { make in
            make.top.equalTo(self.fighterDownloads.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(20)
            make.width.equalTo(mainBounds.width).multipliedBy(0.33)
            make.height.equalTo(50)
        }

        view.addSubview(fighterPrice)
        fighterPrice.snp.remakeConstraints { make in
            make.top.equalTo(self.fighterRate.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.width.equalTo(mainBounds.width * 0.2)
            make.height.equalTo(50)
        }

        view.addSubview(fighterImage)
        fighterImage.snp.remakeConstraints { [unowned self] make in
            make.bottom.equalTo(self.fighterBackground.snp.bottom)
            make.right.equalTo(self.fighterBackground).offset(-20)
            make.width.equalTo(mainBounds.width * 0.5)
            make.height.equalTo(mainBounds.width * 0.5)
        }

        view.addSubview(fighterDescription)
        fighterDescription.snp.remakeConstraints { make in
            make.top.equalTo(self.fighterImage.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
        }
    }

    // MARK: - Navigation

    override func navigationLeftAction(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }

    // MARK: - Update

    func updateWithModel()
    {
        if let model = model
        {
            fighterName.text      = model.name
            fighterUniverse.text  = model.universeName
            fighterDownloads.text = "\(model.downloads) downloads"
            fighterRate.buttonsSelectedTo(index: Int(model.rate - 1))
            fighterPrice.text     = "$\(Int(model.price))"

            model.imageGet(success: { [unowned self] image in
                fighterImage.image = image
            }) { error in
                KMBLog.p(message: "error: \(error)")
            }

            fighterDescription.text = model.descriptionString
            fighterDescription.lineSpacing(size: 20)
        }
    }
}


extension UIViewController
{
    func showKMBFighterDetailVC(model: KMBMFighters)
    {
        let vc   = KMBFighterDetailVC()

        vc.model = model

        navigationController?.pushViewController(vc, animated: true)
    }
}

