//
//  KMBFighterFilterVC.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import WARangeSlider

class KMBFighterFilterVC: KMBBaseVC
{
    var model: KMBFightersFilterModel?

    var filterAppliedClosure: ((KMBFightersFilterModel?) -> ())?

    lazy var sortByLabel: UILabel = {

        let label = UILabel()

        label.font      = KMBFont.helveticaNeueBold.asFont(size: 16)
        label.textColor = KMBColor.black.asColor()
        label.text      = "Sort By"

        return label
    }()

    lazy var sortByAscendingOption: KMBHorizontalButton = {

        let hButton = KMBHorizontalButton()

        hButton.titleLabel.text = "Ascending"
        hButton.isSelected      = true
        hButton.button.tag      = 0

        hButton.buttonClosure = { [unowned self] option in
            updateSortByOption(option: option)
        }

        return hButton
    }()

    lazy var sortByDescendingOption: KMBHorizontalButton = {

        let hButton = KMBHorizontalButton()

        hButton.titleLabel.text = "Descending"
        hButton.isSelected      = true
        hButton.button.tag      = 1

        hButton.buttonClosure = { [unowned self] option in
            updateSortByOption(option: option)
        }

        return hButton
    }()

    lazy var sortByRateOption: KMBHorizontalButton = {

        let hButton = KMBHorizontalButton()

        hButton.titleLabel.text = "Rate"
        hButton.isSelected      = true
        hButton.button.tag      = 2

        hButton.buttonClosure = { [unowned self] option in
            updateSortByOption(option: option)
        }

        return hButton
    }()

    lazy var sortByDownloadsOption: KMBHorizontalButton = {

        let hButton = KMBHorizontalButton()

        hButton.titleLabel.text = "Downloads"
        hButton.isSelected      = true
        hButton.button.tag      = 3

        hButton.buttonClosure = { [unowned self] option in
            updateSortByOption(option: option)
        }

        return hButton
    }()

    lazy var separatorPriceView: UIView = {

        let view = UIView()

        view.backgroundColor = KMBColor.separatorGray.asColor().withAlphaComponent(0.3)

        return view
    }()

    lazy var priceLabel: UILabel = {

        let label = UILabel()

        label.font      = KMBFont.helveticaNeueBold.asFont(size: 16)
        label.textColor = KMBColor.black.asColor()
        label.text      = "Price"

        return label
    }()


    lazy var rangeSliderMinValueLabel: UILabel = {

        let label = UILabel()

        label.font      = KMBFont.helveticaNeueNormal.asFont(size: 12)
        label.textColor = KMBColor.black.asColor()

        return label
    }()

    lazy var rangeSliderMaxValueLabel: UILabel = {

        let label = UILabel()

        label.font      = KMBFont.helveticaNeueNormal.asFont(size: 12)
        label.textColor = KMBColor.black.asColor()

        return label
    }()

    lazy var rangeSlider: RangeSlider = {

        var sliderframe = mainBounds
        sliderframe.size.height = 50

        let slider = RangeSlider(frame: sliderframe)

        slider.minimumValue = 0
        slider.maximumValue = 1000

        slider.lowerValue = 0
        slider.upperValue = 1000

        slider.addTarget(self, action: #selector(rangeSliderAction(_:)), for: .valueChanged)

        return slider
    }()

    lazy var separatorStarsView: UIView = {

        let view = UIView()

        view.backgroundColor = KMBColor.separatorGray.asColor().withAlphaComponent(0.3)

        return view
    }()

    lazy var starsLabel: UILabel = {

        let label = UILabel()

        label.font      = KMBFont.helveticaNeueBold.asFont(size: 16)
        label.textColor = KMBColor.black.asColor()
        label.text      = "Stars"

        return label
    }()

    lazy var startRateView: KMBRateView = {

        let rateView = KMBRateView(width: mainBounds.width * 0.8, mode: .orange)

        return rateView
    }()

    lazy var continueButton: UIButton = {

        let button = UIButton()

        button.redSelectedStyle()
        button.setTitle("Apply Filters", for: .normal)
        button.addTarget(self, action: #selector(continueButtonAction(_:)), for: .touchUpInside)

        return button
    }()

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setup()
    }

    // MARK: - Deinit

    deinit {
        KMBLog.p(message: "\(type(of: self)) deinit")
    }

    // MARK: - Setup

    func viewSetup()
    {
        navigationSetup()
        navigationRightButtonHide()

        navigationLeftButton(image: UIImage(systemName: "chevron.backward"))

        navigationItem.backButtonTitle = ""
        navigationItem.leftBarButtonItem?.tintColor = KMBColor.white.asColor()

        title = "Filters"
    }

    func setup()
    {
        viewSetup()

        view.addSubview(sortByLabel)
        sortByLabel.snp.remakeConstraints { make in
            make.top.equalToSuperview() //.offset(topBarHeight)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(sortByAscendingOption)
        sortByAscendingOption.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.sortByLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(50)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(sortByDescendingOption)
        sortByDescendingOption.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.sortByAscendingOption.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(50)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(sortByRateOption)
        sortByRateOption.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.sortByDescendingOption.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(50)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(sortByDownloadsOption)
        sortByDownloadsOption.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.sortByRateOption.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(50)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(separatorPriceView)
        separatorPriceView.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.sortByDownloadsOption.snp.bottom).offset(20)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(1)
        }

        view.addSubview(priceLabel)
        priceLabel.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.separatorPriceView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(rangeSliderMinValueLabel)
        rangeSliderMinValueLabel.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.priceLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.width.greaterThanOrEqualTo(0)
            make.height.equalTo(40)
        }

        view.addSubview(rangeSliderMaxValueLabel)
        rangeSliderMaxValueLabel.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.priceLabel.snp.bottom).offset(10)
            make.right.equalToSuperview().offset(-20)
            make.width.greaterThanOrEqualTo(0)
            make.height.equalTo(40)
        }

        view.addSubview(rangeSlider)
        rangeSlider.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.rangeSliderMinValueLabel.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(50)
        }
        rangeSliderGetValues()

        view.addSubview(separatorStarsView)
        separatorStarsView.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.rangeSlider.snp.bottom).offset(20)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(1)
        }

        view.addSubview(starsLabel)
        starsLabel.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.separatorStarsView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }

        view.addSubview(startRateView)
        startRateView.snp.remakeConstraints { [unowned self] make in
            make.top.equalTo(self.starsLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(30)
            make.right.equalToSuperview().offset(-30)
            make.height.equalTo(50)
        }

        view.addSubview(continueButton)
        continueButton.snp.remakeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-40)
            make.height.equalTo(50)
        }
    }

    // MARK: - Navigation

    override func navigationLeftAction(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }

    // MARK: - Slider

    func rangeSliderGetValues()
    {
        rangeSliderMinValueLabel.text = "$\(Int(rangeSlider.lowerValue))"
        rangeSliderMaxValueLabel.text = "$\(Int(rangeSlider.upperValue))"
    }


    // MARK: - Actions

    @objc func continueButtonAction(_ sender: Any)
    {
        updateFilterModel()
        filterAppliedClosure?(model)
        navigationLeftAction(self)
    }

    @objc func rangeSliderAction(_ sender: Any)
    {
        rangeSliderGetValues()
    }


    // MARK: - Sort By

    func updateSortOptionsAllUnselected()
    {
        sortByAscendingOption.isSelected  = false
        sortByDescendingOption.isSelected = false
        sortByRateOption.isSelected       = false
        sortByDownloadsOption.isSelected  = false
    }

    func updateSortByOption(option: Int)
    {
        updateSortOptionsAllUnselected()

        switch option
        {
        case 0:
            sortByAscendingOption.isSelected   = true
            model?.sortBy = .asc

        case 1:
            sortByDescendingOption.isSelected  = true
            model?.sortBy = .desc

        case 2:
            sortByRateOption.isSelected        = true
            model?.sortBy = .rate

        case 3:
            sortByDownloadsOption.isSelected   = true
            model?.sortBy = .downloads

        default: break
        }
    }


    // MARK: - Update

    func updateFilterModel()
    {
        model?.priceMin = rangeSlider.lowerValue
        model?.priceMax = rangeSlider.upperValue

        model?.stars = startRateView.getValue()
    }

    func updateWithModel()
    {
        if let model = model
        {
            switch model.sortBy
            {
            case .asc:
                updateSortByOption(option:0)

            case .desc:
                updateSortByOption(option:1)

            case .rate:
                updateSortByOption(option:2)

            case .downloads:
                updateSortByOption(option:3)
            }

            rangeSlider.lowerValue = model.priceMin
            rangeSlider.upperValue = model.priceMax

            if model.stars == 0
            {
                startRateView.buttonsAllToNormal()
            }
            else
            {
                startRateView.buttonsSelectedTo(index: model.stars - 1)
            }
        }
    }
}

extension UIViewController
{
    func showKMBFighterFilterVC(model: KMBFightersFilterModel, filterApplied: ((KMBFightersFilterModel?) -> ())? = nil)
    {
        let vc   = KMBFighterFilterVC()

        vc.filterAppliedClosure = filterApplied

        vc.model = model
        vc.updateWithModel()

        navigationController?.pushViewController(vc, animated: true)
    }
}

