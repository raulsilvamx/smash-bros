//
//  KMBMainNavigationVC.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBMainNavigationVC: UINavigationController
{
    init(vc: UIViewController)
    {
        super.init(rootViewController: vc)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(rootViewController: UIViewController())
        setup()
    }

    func setup()
    {
        view.backgroundColor = KMBColor.white.asColor()
    }
}
