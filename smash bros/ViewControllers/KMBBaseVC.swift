//
//  KMBBaseVC.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//

import UIKit
import Alamofire
import AERecord

class KMBBaseVC: UIViewController
{
    private var activityIndicatorView = KMBActivityIndicator()

    // MARK: - View Cycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = KMBColor.white.asColor()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

    }

    // MARK: - Activity Indicator

    func activityIndicatorShow()
    {
        mainThread { [unowned self] in
            view.addActivityIndicator(view: activityIndicatorView)
            activityIndicatorView.show()
        }
    }

    func activityIndicatorHide()
    {
        activityIndicatorView.hide()
    }

    // MARK: - Navigation bar

    func navigationSetup()
    {
        let nav = self.navigationController?.navigationBar

        nav?.isTranslucent         = false
        nav?.barStyle              = UIBarStyle.black
        nav?.backgroundColor       = KMBColor.white.asColor()
        nav?.tintColor             = KMBColor.black.asColor()
        nav?.titleTextAttributes   = [NSAttributedString.Key.foregroundColor: KMBColor.black.asColor(),
                                      NSAttributedString.Key.font: KMBFont.helveticaNeueMedium.asFont(size: 20)]

        navigationController?.isNavigationBarHidden = false
        navigationController?.view.backgroundColor  = KMBColor.white.asColor()
    }

    func navigationTransparentSetup()
    {
        let nav = self.navigationController?.navigationBar

        nav?.isTranslucent         = true
        nav?.barStyle              = UIBarStyle.black
        nav?.shadowImage           = UIImage()
        nav?.backgroundColor       = .clear
        nav?.barTintColor          = .white
        nav?.tintColor             = .white
        nav?.titleTextAttributes   = [NSAttributedString.Key.foregroundColor: KMBColor.white.asColor(),
                                      NSAttributedString.Key.font: KMBFont.helveticaNeueMedium.asFont(size: 20)]

        nav?.setBackgroundImage(UIImage(), for: .default)

        navigationController?.view.backgroundColor  = .clear
        navigationController?.isNavigationBarHidden = false
    }

    func navigationHide()
    {
        navigationController?.isNavigationBarHidden = true
    }

    func navigationLeftButtonHide()
    {
        navigationItem.leftBarButtonItem = nil
    }

    func navigationLeftButton(image: UIImage? = nil)
    {
        var img : UIImage?

        if image != nil
        {
            img = image?.withRenderingMode(.alwaysOriginal)
        }

        navigationItem.leftBarButtonItem = UIBarButtonItem(image  : img,
                                                           style  : .plain,
                                                           target : self,
                                                           action : #selector(navigationLeftAction(_:)))
    }

    @objc func navigationLeftAction(_ sender: Any) {}


    func navigationRightButtonHide()
    {
        navigationItem.rightBarButtonItem = nil
    }

    func navigationRightButton(image: UIImage? = nil)
    {
        var img : UIImage?

        if image != nil
        {
            img = image?.withRenderingMode(.alwaysOriginal)
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(image  : img,
                                                            style  : .plain,
                                                            target : self,
                                                            action : #selector(navigationRightAction(_:)))
    }

    @objc func navigationRightAction(_ sender: Any) {}


    // MARK: - Getters

    func universeTest()
    {
        if let universes = KMBMUniverses.all() as? [KMBMUniverses]
        {
            var sorts = KMBFightersSorterModel()
            sorts.sorters = [.name, .price, .downloads]

            fightersGet(universe    : universes[2].name,
                        sortedModel : sorts) { response in

                for fighter in response
                {
                    KMBLog.p(message: "Fighter name      :  \(fighter.name ?? "")")
                    KMBLog.p(message: "Fighter price     :  \(fighter.price)")
                    KMBLog.p(message: "Fighter downloads :  \(fighter.downloads)")
                    KMBLog.p(message: "")
                }

            } failure: { error in
                KMBLog.p(message: "fightersGet error \(error)")
            }
        }
    }

    // MARK: - Core Data

    func coreDataFighterSave(model: KMBFighterModel)
    {
        if let objectId = model.objectId
        {
            let fighter = KMBMFighters.firstOrCreate(with: "objectId", value: objectId)
            fighter.fill(model: model)

            AERecord.save()
        }
    }

    // MARK: - Cache

    func universesAndFightersToCacheFreshData(success : (() -> ())? = nil,
                                              failure : ((KMBAPIError) -> ())? = nil)
    {
        AERecord.truncateAllData()
        AERecord.saveAndWait()

        KMBFile.imageCachedDeleteAll()

        universesAndFightersToCache(success: success,
                                    failure: failure)
    }

    func universesAndFightersToCache(success : (() -> ())? = nil,
                                     failure : ((KMBAPIError) -> ())? = nil)
    {
        universesGetAll { [unowned self] response in

            KMBLog.p(message: "universesGetAll count: \(response.count)")

            fightersGetAll { response in

                KMBLog.p(message: "fightersGetAll count: \(response.count)")
                success?()

            } failure: { error in
                KMBLog.p(message: "fightersGetAll error: \(error)")
                failure?(error)
            }

        } failure: { error in
            KMBLog.p(message: "universesGetAll error: \(error)")
            failure?(error)
        }
    }

    func universesGetAll(success : (([KMBMUniverses]) -> ())? = nil,
                         failure : ((KMBAPIError) -> ())? = nil)
    {
        if let universes = KMBMUniverses.all() as? [KMBMUniverses]
        {
            success?(universes)
        }
        else
        {
            requestUniversesGet { response in

                // Cache

                for (index, model) in response.enumerated()
                {
//                    KMBLog.p(message: "objectId index: \(index) - \(model.objectId ?? "")")

                    let objectId = UUID().uuidString

                    let universe = KMBMUniverses.firstOrCreate(with: "objectId", value: objectId)
                    universe.fill(model: model)
                    universe.objectId = objectId
                }

                AERecord.saveAndWait()

                if let universes = KMBMUniverses.all() as? [KMBMUniverses]
                {
                    success?(universes)
                }
                else
                {
                    success?([])
                }

            } failure: { error in
                failure?(error)
            }
        }
    }

    func fightersGetAll(success : (([KMBMFighters]) -> ())? = nil,
                        failure : ((KMBAPIError) -> ())? = nil)
    {
        if let fighters = KMBMFighters.all() as? [KMBMFighters]
        {
            success?(fighters)
        }
        else
        {
            requestFightersGet { response in

                // Cache

                for (index, model) in response.enumerated()
                {
//                    KMBLog.p(message: "objectId index: \(index) - \(model.objectId ?? "")")

                    let objectId = UUID().uuidString

                    let fighter = KMBMFighters.firstOrCreate(with: "objectId", value: objectId)

                    fighter.fill(model: model)
                    fighter.objectId = objectId

                    fighter.imageGet { image in
                        KMBLog.p(message: "fighter id: \(fighter.objectId ?? "") - image cached!")
                    } failure: { error in
                        KMBLog.p(message: "fighter id: \(fighter.objectId ?? "") - image cached error!")
                    }
                }

                AERecord.saveAndWait()

                if let fighters = KMBMFighters.all() as? [KMBMFighters]
                {
                    success?(fighters)
                }
                else
                {
                    success?([])
                }

            } failure: { error in
                failure?(error)
            }
        }
    }

    func fightersGet(universe    : String? = nil,
                     sortedModel : KMBFightersSorterModel? = nil,
                     success     : (([KMBMFighters]) -> ())? = nil,
                     failure     : ((KMBAPIError) -> ())? = nil)
    {
        var orderedBy : [NSSortDescriptor]?

        if let sortedModel = sortedModel
        {
            orderedBy = sortedModel.toSortDescriptor()
        }

        if let universe = universe
        {
            let predicate = KMBMFighters.createPredicate(with: ["universeName" : universe])

            if let fighters = KMBMFighters.all(with: predicate, orderedBy: orderedBy) as? [KMBMFighters]
            {
                success?(fighters)
            }
            else
            {
                let error = KMBAPIError()
                failure?(error)
            }
        }
        else
        {
            if let fighters = KMBMFighters.all(orderedBy: orderedBy) as? [KMBMFighters]
            {
                success?(fighters)
            }
            else
            {
                let error = KMBAPIError()
                failure?(error)
            }
        }
    }

    // MARK: - Requests

    func requestUniversesGet(success : (([KMBUniverseModel]) -> ())? = nil,
                             failure : ((KMBAPIError) -> ())? = nil)
    {
        //activityIndicatorShow()

        delay(1) { [unowned self] in

            KMBAPI.universesGet(success: { [unowned self] response in
                //activityIndicatorHide()
                success?(response)
            },
            failure: { [unowned self] error in
                //activityIndicatorHide()
                failure?(error)
            })
        }
    }

    func requestFightersGet(universe : String? = nil,
                            success  : (([KMBFighterModel]) -> ())? = nil,
                            failure  : ((KMBAPIError) -> ())? = nil)
    {
        var params : [String : String] = [:]

        if universe != nil
        {
            params["universe"] = universe
        }

        //activityIndicatorShow()

        delay(1) { [unowned self] in

            KMBAPI.fightersGet(params: params,
            success: { [unowned self] response in
                //activityIndicatorHide()
                success?(response)
            },
            failure: { [unowned self] error in
                //activityIndicatorHide()
                failure?(error)
            })
        }
    }
}

