//
//  KMBFightersFilterModel.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

enum KMBFilterSortBy : Int
{
    case asc
    case desc
    case rate
    case downloads
}

struct KMBFightersFilterModel
{
    var universe : String?

    var sortBy   : KMBFilterSortBy = .asc

    var priceMin : Double = 0
    var priceMax : Double = 1000

    var stars    : Int    = 0

    func toPredicate() -> NSPredicate?
    {
        var predicates : [NSPredicate] = []

        // Universe

        if universe != nil
        {
            predicates.append(NSPredicate(format: "(universeName == %@)", universe!))
        }

        // Price

        if priceMin != 0 ||
           priceMax != 0
        {
            predicates.append(NSPredicate(format: "(price >= %f) AND (price <= %f)", priceMin, priceMax))
        }

        // Rate

        if stars > 0
        {
            predicates.append(NSPredicate(format: "(rate == %i)", stars))
        }

        if predicates.count > 0
        {
            let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            return compoundPredicate
        }

        return nil
    }

    func toSortBy() -> [NSSortDescriptor]
    {
        switch sortBy
        {
        case .asc:
            return [NSSortDescriptor(key: "name", ascending: true)]

        case .desc:
            return [NSSortDescriptor(key: "name", ascending: false)]

        case .rate:
            return [NSSortDescriptor(key: "rate", ascending: true)]

        case .downloads:
            return [NSSortDescriptor(key: "downloads", ascending: true)]
        }
    }
}
