//
//  KMBUICollectionView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension UICollectionView
{
    func reloadDataMainThread()
    {
        mainThread { [unowned self] in
            reloadData()
        }
    }
}
