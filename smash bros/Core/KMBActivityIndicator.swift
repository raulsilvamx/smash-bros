//
//  KMBActivityIndicator.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import SnapKit

class KMBActivityIndicator: UIView
{
    lazy var activityBoxView : UIView = {

        let view  = UIView(frame: CGRect(x      : 0,
                                         y      : 0,
                                         width  : 80,
                                         height : 80))

        view.backgroundColor    = UIColor.white.withAlphaComponent(1)
        view.clipsToBounds      = true
        view.roundedStyle(round: 19)

        return view
    }()

    lazy var activityIndicatorView : UIActivityIndicatorView = {

        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)

        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()

        return activityIndicator
    }()


    // MARK: - Init

    init()
    {
        super.init(frame: mainBounds)
        setup()
    }

    /// Init for using in code
    /// - Parameter frame: CGRect
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    /// Init for using in IB
    /// - Parameter coder: NSCoder
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        setup()
    }

    // MARK: - Setup

    func viewSetup()
    {
        backgroundColor = .clear
    }

    func setup()
    {
        viewSetup()

        addSubview(activityBoxView)
        activityBoxView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalTo(80)
        }

        activityBoxView.addSubview(activityIndicatorView)
        activityIndicatorView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    func hide()
    {
        mainThread { [unowned self] in
            self.isHidden = true
            self.activityIndicatorView.stopAnimating()
        }
    }

    func show()
    {
        mainThread { [unowned self] in
            self.isHidden = false
            self.activityIndicatorView.startAnimating()
        }
    }
}

extension UIView
{
    func addActivityIndicator(view: KMBActivityIndicator)
    {
        addSubview(view)
        view.snp.updateConstraints { make in
            make.left.top.right.bottom.equalToSuperview()
        }
    }
}
