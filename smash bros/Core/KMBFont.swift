//
//  KMBFont.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

enum KMBFont: String
{
    /**
     family: Helvetica Neue
     font: HelveticaNeue
     font: HelveticaNeue-Italic
     font: HelveticaNeue-UltraLight
     font: HelveticaNeue-UltraLightItalic
     font: HelveticaNeue-Thin
     font: HelveticaNeue-ThinItalic
     font: HelveticaNeue-Light
     font: HelveticaNeue-LightItalic
     font: HelveticaNeue-Medium
     font: HelveticaNeue-MediumItalic
     font: HelveticaNeue-Bold
     font: HelveticaNeue-BoldItalic
     font: HelveticaNeue-CondensedBold
     font: HelveticaNeue-CondensedBlack
     */
    case helveticaNeueBold    = "HelveticaNeue-Bold"
    case helveticaNeueMedium  = "HelveticaNeue-Medium"
    case helveticaNeueNormal  = "HelveticaNeue"

    func asFont(size: CGFloat = 12) -> UIFont
    {
        return UIFont(name: self.rawValue, size: size)!
    }

    static func fontList()
    {
        for family in UIFont.familyNames
        {
            KMBLog.p(message: "family: \(family)")
            for font in UIFont.fontNames(forFamilyName: family)
            {
                KMBLog.p(message: "font: \(font)")
            }
        }
    }
}
