//
//  KMBColor.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

enum KMBColor: String
{
    case main       = "#DB3069"
    case black      = "#000000"
    case white      = "#FFFFFF"

    case borderGray     = "#FBFBFB"
    case borderGrayBack = "#E1E1E1"
    case separatorGray  = "#979797"
    case fighterImageBg = "#FBFBFA"
    case fighterText    = "#343434"

    case starOrange     = "#FFCD00"

    case filterSelected   = "#64C328"
    case filterUnselected = "#A4AAB3"

    func asColor() -> UIColor
    {
        return UIColor.hexStringToUIColor(hex: self.rawValue)
    }
}

extension UIColor
{
    static func hexStringToUIColor (hex: String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red   : CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green : CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue  : CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha : CGFloat(1.0)
        )
    }
}
