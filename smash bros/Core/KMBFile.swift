//
//  KMBFile.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 07/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import FileKit

public enum KMBFileDirectory: String
{
    case imagesCache
}

class KMBFile: NSObject
{
    // MARK: - Directory

    static func directory(_ directory: KMBFileDirectory = .imagesCache) -> Path?
    {
        let pathString: Path = .userDocuments + directory.rawValue

        do {
            try pathString.createDirectory()
            return pathString
        } catch {
            KMBLog.p(message: "\(type(of: self)) \(#function) error: \(error.localizedDescription)")
        }

        return nil
    }

    static func clearDir(path: Path, ext: [String]? = nil)
    {
        KMBLog.p(message: "\(type(of: self)) \(#function)")

        let files = path.files(containtString: ext, containtType: .last)

        if files.count > 0
        {
            for file in files
            {
                do {
                    KMBLog.p(message: "deleting: \(file.fileName)")
                    try file.deleteFile()
                } catch {
                    KMBLog.p(message: "\(type(of: self)) \(#function) error: \(error.localizedDescription)")
                }
            }
        }
    }

    static func imageCachedDeleteAll()
    {
        self.clearDir(path: imagesPath, ext: [".png"])
    }
}

enum PathOrderType: Int
{
    case nameAsc = 0
    case nameDesc
    case creationDateAsc
    case creationDateDesc
    case modificationDateAsc
    case modificationDateDesc
}

enum PathContaintType: Int
{
    case exactly = 0
    case containt
    case first
    case last
}

extension Path
{
    func files(containtString: [String]?        = nil,
               containtType:   PathContaintType = .exactly,
               order:          PathOrderType    = .nameAsc) -> [Path]
    {
        if self.isDirectory
        {
            var paths =  self.find(searchDepth: 1) { path in

                if containtString != nil
                {
                    var toList = false

                    let filenameWithExt = path.fileName

                    for string in containtString!
                    {
                        switch containtType
                        {
                        case .exactly:
                            if filenameWithExt == string
                            {
                                toList = true
                            }
                            break

                        case .containt:
                            if filenameWithExt.contains(string)
                            {
                                toList = true
                            }
                            break

                        case .first:
                            if filenameWithExt.hasPrefix(string)
                            {
                                toList = true
                            }
                            break

                        case .last:
                            if filenameWithExt.hasSuffix(string)
                            {
                                toList = true
                            }
                            break
                        }
                    }

                    return toList
                }

                return true
            }

            paths.sort { path1, path2 in

                switch order
                {
                case .nameAsc:
                    return path1.fileName < path2.fileName

                case .nameDesc:
                    return path1.fileName > path2.fileName

                case .creationDateAsc:
                    return path1.creationDate! > path2.creationDate!

                case .creationDateDesc:
                    return path1.creationDate! < path2.creationDate!

                case .modificationDateAsc:
                    return path1.modificationDate! > path2.modificationDate!

                case .modificationDateDesc:
                    return path1.modificationDate! < path2.modificationDate!
                }
            }

            return paths
        }

        return []
    }
}
