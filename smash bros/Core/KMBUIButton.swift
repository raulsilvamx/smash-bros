//
//  KMBUIButton.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 08/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBUIButton: NSObject {

}

extension UIButton
{
    func redSelectedStyle()
    {
        borderClean()
        roundedStyle()

        backgroundColor = KMBColor.main.asColor()
        setTitleColor(KMBColor.white.asColor(), for: .normal)
    }

    func redNormalStyle()
    {
        roundedStyle()
        borderMain()

        backgroundColor = KMBColor.white.asColor()
        setTitleColor(KMBColor.main.asColor(), for: .normal)
    }

    func startStyle()
    {
        let image = UIImage(systemName: "star.fill")?.withRenderingMode(.alwaysTemplate)

        setImage(image, for: .normal)
        setImage(image, for: .selected)
    }
}
