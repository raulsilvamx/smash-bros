//
//  KMBBundle.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension Bundle
{
    var appVersionNumber: String?
    {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }

    var appBuildNumber: String?
    {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
