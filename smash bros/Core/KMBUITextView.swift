//
//  KMBUITextView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension UITextView
{
    func lineSpacing(size: CGFloat = 12)
    {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = size

        let attributes = [NSAttributedString.Key.paragraphStyle: style]
        attributedText = NSAttributedString(string: text, attributes: attributes)
    }
}
