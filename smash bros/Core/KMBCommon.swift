//
//  KMBCommon.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

// mogenerator --model smash_bros.xcdatamodeld --output-dir CoreData --swift

import UIKit

/// Main bounds
let mainBounds = UIScreen.main.bounds

// MARK: - Threads

/// Main thread to execute closure
/// - Parameter execute: closure to execute
func mainThread(execute: @escaping () -> Void)
{
    DispatchQueue.main.async(execute: execute)
}

/// Delay to execute closure
/// - Parameters:
///   - delay: time in seconds
///   - closure: closure to execute
func delay(_ delay:Double, closure:@escaping ()->())
{
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func sceneMain() -> SceneDelegate?
{
    let scene = UIApplication.shared.connectedScenes.first

    if let sd = (scene?.delegate as? SceneDelegate)
    {
        return sd
    }

    return nil
}
