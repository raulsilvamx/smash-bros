//
//  KMBString.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension String
{
    // MARK: - App Values

    /// App's environment
    static func appEnvironment() -> String
    {
        #if RELEASE
            return ""

        #elseif DEBUG
            return "DEBUG"

        #endif
    }

    /// App's version and build
    static func appVersionBuild() -> String
    {
        return "\(Bundle.main.appVersionNumber ?? "?") (\(Bundle.main.appBuildNumber ?? "?"))"
    }

    /// App's version, build and environment
    static func appVersionBuildWithEnvironment() -> String
    {
        return "\(String.appVersionBuild()) \(String.appEnvironment())"
    }

    /// App's actual target name
    static func appTargetName() -> String
    {
        return Bundle.main.infoDictionary?["CFBundleName"] as! String
    }

    func attributedValues(font:           UIFont  = KMBFont.helveticaNeueNormal.asFont(),
                          size:           CGFloat = 16,
                          color:          UIColor = KMBColor.black.asColor(),
                          verticalAdjust: CGFloat = 0) -> [NSAttributedString.Key : Any]
    {
        return [NSAttributedString.Key.font            : font as Any,
                NSAttributedString.Key.foregroundColor : color as Any,
                NSAttributedString.Key.baselineOffset  : verticalAdjust]
    }

    func textBlackBold(size: CGFloat = 12) -> NSAttributedString
    {
        let values = attributedValues(font  : KMBFont.helveticaNeueBold.asFont(),
                                      size  : size,
                                      color : KMBColor.black.asColor())

        return NSAttributedString(string:     self,
                                  attributes: values)
    }

    func textBlackNormal(size: CGFloat = 12) -> NSAttributedString
    {
        let values = attributedValues(font  : KMBFont.helveticaNeueNormal.asFont(),
                                      size  : size,
                                      color : KMBColor.black.asColor())

        return NSAttributedString(string:     self,
                                  attributes: values)
    }
}
