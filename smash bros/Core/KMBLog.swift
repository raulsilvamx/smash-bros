//
//  KMBLog.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

class KMBLog: NSObject
{
    static func p(message : String,
                  force   : Bool = false)
    {
        var show = false

        // To enable in a certain environment go to Building Setting -> Swift Other Flags add -DLOGS
        #if LOGS
            show = true
        #endif

        if force
        {
            show = true
        }

        if show
        {
            print(message)
        }
    }

    static func separador(force: Bool = false)
    {
        self.p(message: "===========================================================", force: force)
    }

    static func dictionary(message    : String,
                           dictionary : [String : AnyObject],
                           force      : Bool = false)
    {
        self.p(message : message,
               force   : force)

        for (key, value) in dictionary
        {
            self.p(message : "\(key) : \(value)",
                   force   : force)
        }
    }
}
