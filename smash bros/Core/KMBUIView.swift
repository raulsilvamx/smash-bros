//
//  KMBUIView.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 05/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension UIView
{
    // MARK: - Layers

    func removeLayers()
    {
        layer.sublayers?.removeAll()
    }

    // MARK: - Rounder Style

    func roundedStyle(round: CGFloat = 8)
    {
        layer.cornerRadius  = round
        layer.masksToBounds = true
    }

    // MARK: - Border

    func borderClean()
    {
        layer.borderWidth = 0
        layer.borderColor = UIColor.clear.cgColor
    }

    func borderMain()
    {
        layer.borderWidth = 2
        layer.borderColor = KMBColor.main.asColor().cgColor
    }

    func borderGrayLight()
    {
        layer.borderWidth = 1
        layer.borderColor = KMBColor.borderGrayBack.asColor().cgColor
    }
}
