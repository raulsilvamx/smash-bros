//
//  KMBUIViewController.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension UIViewController
{
    var topBarHeight: CGFloat
    {
        var top = self.navigationController?.navigationBar.frame.height ?? 0.0

        if #available(iOS 13.0, *) {
            top += UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            top += UIApplication.shared.statusBarFrame.height
        }

        return top
    }
}
