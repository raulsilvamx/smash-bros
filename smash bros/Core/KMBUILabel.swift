//
//  KMBUILabel.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 09/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit

extension UILabel
{
    func fighter(name: String, universe: String)
    {
        let string = NSMutableAttributedString()

        string.append(name.textBlackBold())
        string.append("\n".textBlackBold())
        string.append(universe.textBlackNormal())

        attributedText = string
    }
}
