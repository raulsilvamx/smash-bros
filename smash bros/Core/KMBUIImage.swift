//
//  KMBUIImage.swift
//  smash bros
//
//  Created by Raul Silva Delgado on 06/01/22.
//  Copyright © 2022 Koombea. All rights reserved.
//

import UIKit
import FileKit

var imagesPath : Path {
    return KMBFile.directory(.imagesCache)!
}

func imagePathFilenamePNG(id: String) -> Path
{
    return imagesPath + "\(id).png"
}

extension UIImage
{
    func saveToCache(id: String)
    {
        if let pngImageData = self.pngData()
        {
            try? pngImageData.write(to: imagePathFilenamePNG(id: id))
        }
    }

    static func getFromCache(id: String) -> UIImage?
    {
        let path = imagePathFilenamePNG(id: id)

        if path.exists
        {
            if let imageData = try? Data.read(from: path)
            {
                if let image = UIImage(data: imageData)
                {
                    return image
                }
            }
        }

        return nil
    }
}
